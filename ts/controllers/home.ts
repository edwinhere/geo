/// <reference path="../index.ts" />
module quibbler {
    "use strict";
    const app = getModule();

    interface IListing {
    }

    class HomeController {
        private list: IListing[] = [] as IListing[];
        private loading: boolean;

        constructor(private $http: ng.IHttpService) {
        }
        public static $inject: string[] = ["$http"];
    }
    app.controller("HomeController", HomeController);
}