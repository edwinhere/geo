module quibbler {
    "use strict";
    angular.module("geo", ["ngRoute", "ngMaterial", "ngSanitize"]);
    export var getModule: () => ng.IModule = () => {
        return angular.module("geo");
    }
}