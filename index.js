var quibbler;
(function (quibbler) {
    "use strict";
    angular.module("geo", ["ngRoute", "ngMaterial", "ngSanitize"]);
    quibbler.getModule = function () {
        return angular.module("geo");
    };
})(quibbler || (quibbler = {}));
/// <reference path="../index.ts" />
var quibbler;
(function (quibbler) {
    "use strict";
    var app = quibbler.getModule();
    var HomeController = (function () {
        function HomeController($http) {
            this.$http = $http;
            this.list = [];
        }
        return HomeController;
    }());
    HomeController.$inject = ["$http"];
    app.controller("HomeController", HomeController);
})(quibbler || (quibbler = {}));
var quibbler;
(function (quibbler) {
    "use strict";
    var app = quibbler.getModule();
    app.config([
        "$routeProvider", function ($routeProvider) {
            $routeProvider.when("/", {
                templateUrl: "views/home.html",
                controller: "HomeController",
                controllerAs: "ctrl"
            }).when("/about", {
                templateUrl: "views/about.html"
            }).when("/contact", {
                templateUrl: "views/contact.html"
            });
        }
    ]);
})(quibbler || (quibbler = {}));
